package main

const (
	version = "v0.4.0"
)

var (
	gitVersion     = "unset-gitVersion"
	buildTimestamp = "unset-buildTimestamp"
)
