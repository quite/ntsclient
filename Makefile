BINARY := ntsclient
SRCS := client.go go.mod go.sum

GITVERSION != git describe --tags --long --dirty
BUILDTIMESTAMP != date +%s

LDFLAGS="-X main.gitVersion=$(GITVERSION) -X main.buildTimestamp=$(BUILDTIMESTAMP)"

$(BINARY): $(SRCS)
	go build -ldflags $(LDFLAGS) -o $@

lint:
	golangci-lint run --enable-all

bump-major:
	go run bump-version.go version.go major
bump-minor:
	go run bump-version.go version.go minor
bump-patch:
	go run bump-version.go version.go patch
